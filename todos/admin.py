from django.contrib import admin
from todos.models import TodoList, TodoItem


# Register your models here.
@admin.register(TodoList)
class TodoAdmin(admin.ModelAdmin):
    list_display = ["name", "id", "created_on", "num_tasks"]


@admin.register(TodoItem)
class TodoAdmin(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
